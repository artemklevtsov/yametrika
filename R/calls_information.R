#' @title List of uploads of call information
#'
#' @description
#' Returns a list of uploads of call information.
#'
#' @param counter_id Counter ID.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/offline_conversion/findallcalluploadings-docpage/>
#'
#' @export
#'
#' @family ym_calls_information
#'
ym_calls_information_list <- function(counter_id = getOption("ym.counter_id")) {
  # Check args
  assert_number(counter_id)

  path <- paste("management/v1/counter", counter_id, "offline_conversions/calls_uploadings", sep = "/")
  api_get(path = path)$uploadings
}


#' @title Information about the upload of calls
#'
#' @description
#' Returns information about the specified upload.
#'
#' @param counter_id Counter ID.
#' @param upload_id ID of the upload of calls.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/offline_conversion/findcalluploadingbyid-docpage/>
#'
#' @export
#'
#' @family ym_calls_information
#'
ym_calls_information_info <- function(counter_id = getOption("ym.counter_id"), upload_id) {
  # Check args
  assert_number(counter_id)
  assert_number(upload_id)

  path <- paste("management/v1/counter", counter_id, "offline_conversions/calls_uploading", upload_id, sep = "/")
  api_get(path = path)$uploading
}


#' @title Uploading information about calls
#'
#' @description
#' Uploads information about calls.
#'
#' @param counter_id Counter ID.
#' @param client_id_type Type of user identifiers. Allowed values: `CLIENT_ID`,
#' `USER_ID`.
#' @param file File in CSV format.
#' @param comment Comments.
#' @param new_goal_name The name of a new target for calls if this target doesn't exist. If it exists, the parameter is ignored.
#'
#' @details
#' `file` should be in CSV format, with the first row containing the column
#' headers.
#'
#' Required columns:
#' - `UserId` — Site user ID assigned by the site owner (only for client_id_type = USER_ID)
#' - `ClientId` — Site user ID assigned by Yandex.Metrica (only for client_id_type = CLIENT_ID)
#' - `DateTime` — Unix timestamp of the date and time of the conversion.
#'
#' Optional columns:
#' - `StaticCall` — Whether the call was from a static number (1 — static, 0 — dynamic).
#' - `Price` — Goal price (use a dot as the decimal separator).
#' - `Currency` — Currency in three-letter ISO 4217 format.
#' - `PhoneNumber` — Phone number without spaces (including the country and area code). For example: +79451234567.
#' - `TalkDuration` — Duration of the call, in seconds.
#' - `HoldDuration` — How long the call was on hold, in seconds.
#' - `CallMissed` — Whether the call was missed (1 — missed, 0 — answered).
#' - `Tag` — Any tag. You can use it to mark the call quality, result of the call, and so on. For example: "client wasn't #' happy with price".
#' - `FirstTimeCaller` — First time caller (1 — new caller, 0 — repeat caller).
#' - `URL` — URL the call was tracked from (associated with a page event). For example, this could be a landing page for an #' ad campaign that shows the phone number (PhoneNumber).
#' - `CallTrackerURL` — URL leading to the call tracker interface.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/offline_conversion/uploadcalls-docpage/>
#'
#' <https://yandex.com/support/metrica/data/calls.html>
#'
#' @export
#'
#' @family ym_calls_information
#'
ym_calls_information_upload <- function(counter_id = getOption("ym.counter_id"), client_id_type, file, comment = NULL, new_goal_name = NULL) {
  # Check args
  assert_number(counter_id)
  assert_choice(client_id_type, c("CLIENT_ID", "USER_ID"))
  assert_string(comment, null.ok = TRUE)
  assert_string(new_goal_name, null.ok = TRUE)
  assert_string(file)
  assert_file_exists(file, access = "r", extension = "csv")

  path <- paste("management/v1/counter", counter_id, "offline_conversions/upload_calls", sep = "/")
  query <- list(
    client_id_type = client_id_type,
    comment = comment
  )

  api_upload(path = path, query = query, file = file, type = "text/csv")$uploading
}


#' @title Enabling the extended call tracking window
#'
#' @description
#' Enables the extended call tracking window for the tag.
#'
#' @param counter_id Counter ID.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/offline_conversion/enablecallsextendedthreshold-docpage/>
#'
#' @export
#'
#' @family ym_calls_information
#'
ym_calls_information_extended_window_on <- function(counter_id = getOption("ym.counter_id")) {
  # Check args
  assert_number(counter_id)

  path <- paste("management/v1/counter", counter_id, "offline_conversions/calls_extended_threshold", sep = "/")
  api_post(path = path)$success
}

#' @title Disabling the extended call tracking window
#'
#' @description
#' Disables the extended call tracking window for this counter.
#'
#' @param counter_id Counter ID.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/offline_conversion/disablecallsextendedthreshold-docpage/>
#'
#' @export
#'
#' @family ym_calls_information
#'
ym_calls_information_extended_window_off <- function(counter_id = getOption("ym.counter_id")) {
  # Check args
  assert_number(counter_id)

  path <- paste("management/v1/counter", counter_id, "offline_conversions/calls_extended_threshold", sep = "/")
  api_del(path = path)$success
}
