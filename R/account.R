#' @title List of accounts
#'
#' @description
#' Returns list of accounts, the representative of which is a current user.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/accounts/accounts-docpage/>
#'
#' @export
#'
#' @family ym_account
#'
ym_account_list <- function() {
  path <- "management/v1/accounts"
  api_get(path = path)$accounts
}


#' @title Changing list of accounts
#'
#' @description Changes the list of accounts, the representative of which is a
#' current user. The list of accounts is updated in accordance with the list of
#' usernames in the input structure.
#'
#' @param user_login User login, which has provided full access to its counters
#' for the current user.
#'
#' @note
#' If the user login is not specified in the input structure, the representative of which is a current user, full account access for this user will be cancelled.
#'
#' If the user login is specified in the input structure, not included in the current list of accounts, this user's access to edit the account is NOT granted.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/accounts/updateaccounts-docpage/>
#'
#' @export
#'
#' @family ym_account
#'
ym_account_add <- function(user_login) {
  # Check args
  assert_character(user_login)

  path <- "management/v1/accounts"
  body <- list(
    accounts = data.frame(
      user_login = user_login,
      stringsAsFactors = FALSE
    )
  )
  api_put(path = path, body = body)$accounts
}


#' @title Deleting account
#'
#' @description
#' Deletes the user login from the list of accounts, the representative of which is a current user.
#'
#' @param user_login 	User login from Yandex.Passport, which must be removed
#' from the list of accounts for the current user.
#'
#' @note
#' When deleting the username from the list of accounts, full access to the account will be cancelled.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/accounts/deleteaccount-docpage/>
#'
#' @export
#'
#' @family ym_account
#'
ym_account_del <- function(user_login) {
  # Check args
  assert_string(user_login)

  path <- "management/v1/account"
  query <- list(user_login = user_login)
  api_del(path = path, query = query)$success
}
