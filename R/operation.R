#' @title List of actions
#'
#' @description
#' Returns information about counter actions.
#'
#' @param counter_id Counter ID.
#'
#' @references
#' https://tech.yandex.com/metrika/doc/api2/management/operations/operations-docpage/
#'
#' @import checkmate
#'
#' @export
#'
#' @family ym_operation
#'
ym_operation_list <- function(counter_id = getOption("ym.counter_id")) {
  # Check args
  assert_number(counter_id)

  path <- paste("management/v1/counter", counter_id, "operations", sep = "/")
  api_get(path = path)$operations
}


#' @title Information on action
#'
#' @description
#' Returns information about specified action of counter.
#'
#' @param counter_id Counter ID.
#' @param operation_id Action ID.
#'
#' @references
#' https://tech.yandex.com/metrika/doc/api2/management/operations/operation-docpage/
#'
#' @export
#'
#' @family ym_operation
#'
ym_operation_info <- function(counter_id = getOption("ym.counter_id"), operation_id) {
  # Check args
  assert_number(counter_id)
  assert_number(operation_id)

  path <- paste("management/v1/counter", counter_id, "operation", operation_id, sep = "/")
  api_get(path = path)$operation
}

#' @title Create action
#'
#' @description
#' Creates action for counter.
#'
#' @param counter_id Counter ID.
#' @param action Possible values:
#' @param attr Field for filtration.
#' @param value Value for replacement.
#' @param status Action status.
#'
#' @references
#' https://tech.yandex.com/metrika/doc/api2/management/operations/addoperation-docpage/
#'
#' @export
#'
#' @family ym_operation
#'
ym_operation_add <- function(counter_id = getOption("ym.counter_id"), action, attr, value, status) {
  # Check args
  assert_number(counter_id)

  query <- list(
    action = action,
    attr = attr,
    value = value,
    status = status
  )
  assert_operation(query)

  body <- list(operation = query)

  path <- paste("management/v1/counter", counter_id, "operations", sep = "/")
  api_post(path = path, body = body)$operation
}


#' @title Changing action
#'
#' @description
#' Changes setting of specified counter action.
#'
#' @param counter_id Counter ID.
#' @param operation_id Action ID.
#' @param action Possible values:
#' @param attr Field for filtration.
#' @param value Value for replacement.
#' @param status Action status.
#'
#' @references
#' https://tech.yandex.com/metrika/doc/api2/management/operations/editoperation-docpage/
#'
#' @export
#'
#' @family ym_operation
#'
ym_operation_edit <- function(counter_id = getOption("ym.counter_id"), operation_id, action, attr, value, status) {
  # Check args
  assert_number(counter_id)
  assert_number(operation_id)

  query <- list(
    action = action,
    attr = attr,
    value = value,
    status = status
  )
  assert_operation(query)

  body <- list(operation = query)

  path <- paste("management/v1/counter", counter_id, "operation", operation_id, sep = "/")
  api_put(path = path, body = body)$operation
}


#' @title Deleting action
#'
#' @description
#' Deletes counter action.
#'
#' @param counter_id Counter ID.
#' @param operation_id Action ID.
#'
#' @references
#' https://tech.yandex.com/metrika/doc/api2/management/operations/deleteoperation-docpage/
#'
#' @export
#'
#' @family ym_operation
#'
ym_operation_del <- function(counter_id = getOption("ym.counter_id"), operation_id) {
  # Check args
  assert_number(counter_id)
  assert_number(operation_id)

  path <- paste("management/v1/counter", counter_id, "operation", operation_id, sep = "/")
  api_del(path = path)$success
}
