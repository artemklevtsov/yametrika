% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/calls_information.R
\name{ym_calls_information_extended_window_off}
\alias{ym_calls_information_extended_window_off}
\title{Disabling the extended call tracking window}
\usage{
ym_calls_information_extended_window_off(
  counter_id = getOption("ym.counter_id")
)
}
\arguments{
\item{counter_id}{Counter ID.}
}
\description{
Disables the extended call tracking window for this counter.
}
\references{
\url{https://tech.yandex.com/metrika/doc/api2/management/offline_conversion/disablecallsextendedthreshold-docpage/}
}
\seealso{
Other ym_calls_information: 
\code{\link{ym_calls_information_extended_window_on}()},
\code{\link{ym_calls_information_info}()},
\code{\link{ym_calls_information_list}()},
\code{\link{ym_calls_information_upload}()}
}
\concept{ym_calls_information}
