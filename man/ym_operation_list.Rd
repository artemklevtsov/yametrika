% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/operation.R
\name{ym_operation_list}
\alias{ym_operation_list}
\title{List of actions}
\usage{
ym_operation_list(counter_id = getOption("ym.counter_id"))
}
\arguments{
\item{counter_id}{Counter ID.}
}
\description{
Returns information about counter actions.
}
\references{
https://tech.yandex.com/metrika/doc/api2/management/operations/operations-docpage/
}
\seealso{
Other ym_operation: 
\code{\link{ym_operation_add}()},
\code{\link{ym_operation_del}()},
\code{\link{ym_operation_edit}()},
\code{\link{ym_operation_info}()}
}
\concept{ym_operation}
